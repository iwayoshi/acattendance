# GITおためしプロジェクト

## bitbucketって?

無料でGITリポジトリが使えるサービス。  
無料だとリポジトリに登録できるユーザが5人までという制限があるが、他の機能は有料版と変わらないらしい。
> つまり我々にとって最強


## CUIからGIT触るのはよくわからん

[SourceTree](https://www.sourcetreeapp.com/)を使うとGUIで感覚的に操作できてイイ感じ。  
ちなみに開発元はbitbucketと同じく[Aatlassian](https://www.atlassian.com/)なので1つのアカウントでリポジトリのログインとSourceTreeのログインができる。


## 移住先

GITで本格的に運用するならGitLab。  
どこかのサーバにインストールすることで無料で充実した機能を使える。
